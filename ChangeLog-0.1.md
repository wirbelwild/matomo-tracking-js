# Changes in Bit&Black Matomo Tracking 0.1

## 0.1.0 2020-12-15

### Fixed 

-   A slash at the end of the URL will be ignored.

### Added 

-   Added entry point.

### Changed

-   Moved to TypeScript.