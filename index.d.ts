/*!
 * Bit&Black Matomo Tracking
 *
 * @copyright Copyright (c) Bit&Black
 * @author Tobias Köngeter <hello@bitandblack.com>
 * @link https://www.bitandblack.com
 */
export { MatomoTagManager } from "./src/MatomoTagManager";
export { MatomoTracking } from "./src/MatomoTracking";
