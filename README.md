![NPM License](https://img.shields.io/npm/l/bitandblack-matomo-tracking)
[![npm version](https://badge.fury.io/js/bitandblack-matomo-tracking.svg)](https://badge.fury.io/js/bitandblack-matomo-tracking)

<p align="center">
    <a href="https://www.bitandblack.com" target="_blank">
        <img src="https://www.bitandblack.com/build/images/BitAndBlack-Logo-Full.png" alt="Bit&Black Logo" width="400">
    </a>
</p>

# Bit&Black Matomo Tracking

Allows including the Matomo Tracking or the Matomo Tag Manager with JavaScript.

## Installation 

This library is made for the use with [Node](https://www.npmjs.com/package/bitandblack-matomo-tracking). Add it to your project by running `$ npm install bitandblack-matomo-tracking`. You can of course also use an alternative to NPM.

## Usage 

### Matomo Tracker

Import the Matomo Tracker and initialize it like that:

````javascript
import { MatomoTracking } from "bitandblack-matomo-tracking";

const matomoTracking = new MatomoTracking(
    99,
    "https://www.yoursite.com/matomo",
    {
        trackPageView: true,
        enableLinkTracking: true
    }
);
````

The parameters are:

1.  The page id. This one is required.
2.  The URL of your Matomo instance. This one is also required.
3.  Tracking options. These are optional.

Note: You don't need to add `piwik.php` or `matomo.php` in the url.

### Matomo Tag Manager

Import the Matomo Tag Manager and initialize it like that:

````javascript
import { MatomoTagManager } from "bitandblack-matomo-tracking";

const matomoTagManager = new MatomoTagManager(
    "container_Yolo25", 
    "https://www.yoursite.com/matomo"
);
````

The parameters are:

1.  The container id. This one is required.
2.  The URL of your Matomo instance. This one is also required.
3.  Tracking options. These are optional.

## Help 

If you have any questions feel free to contact us under `hello@bitandblack.com`.

Further information about Bit&Black can be found under [www.bitandblack.com](https://www.bitandblack.com).