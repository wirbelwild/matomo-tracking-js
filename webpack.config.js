const Encore = require("@symfony/webpack-encore");

if (!Encore.isRuntimeEnvironmentConfigured()) {
    Encore.configureRuntimeEnvironment(process.env.NODE_ENV || "dev");
}

const isProduction = Encore.isProduction();

Encore
    .setOutputPath("example/build/")
    .setPublicPath("/")
    .addEntry("example-1-tracker", "./example/assets/example-1-tracker.js")
    .addEntry("example-2-tag-manager", "./example/assets/example-2-tag-manager.js")
    .cleanupOutputBeforeBuild()
    .enableBuildNotifications()
    .enableSourceMaps(!isProduction)
    .enableVersioning(false)
    .enableSassLoader()
    .disableSingleRuntimeChunk()
    .enableTypeScriptLoader()
;

module.exports = Encore.getWebpackConfig();
