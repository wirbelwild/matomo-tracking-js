/**
 * Bit&Black Matomo Tracking
 *
 * @copyright Copyright (c) Bit&Black
 * @author Tobias Köngeter <hello@bitandblack.com>
 * @link https://www.bitandblack.com
 */

declare global {
    interface Window {
        _paq: any;
    }
}

/**
 * Matomo Tracking
 */
class MatomoTracking {
    
    private config: any = {};
    
    /**
     * @param pageId     The Page Id given by Matomo, for example `1`.
     * @param matomoRoot The root URL to Matomo, for example `https://www.mysite.com/matomo/` or `https://matomo.mysite.com/`.
     * @param options    Options to configure the Tracker.
     * @constructor
     */
    constructor(pageId: number, matomoRoot: string, options: object = {})
    {
        matomoRoot = matomoRoot.replace(/\/+$/, "");
        
        this.config.trackPageView = true;
        this.config.enableLinkTracking = true;
        this.config.setTrackerUrl = `${matomoRoot}/matomo.php`;
        this.config.setSiteId = pageId;
        
        const _paq = window._paq || [];
        const scriptElement: HTMLScriptElement = document.createElement("script");
        const head: HTMLHeadElement | null = document.querySelector("head");

        Object.assign(this.config, options);
    
        for (const prop in this.config) {
            if (!this.config.hasOwnProperty(prop)) {
                continue;
            }

            const value = this.config[String(prop)];
            
            if (value === true) {
                _paq.push([prop]);
            } else if (value !== null) {
                _paq.push([prop, value]);
            }
        }

        window._paq = _paq;

        scriptElement.type = "text/javascript";
        scriptElement.async = true;
        scriptElement.src = `${matomoRoot}/matomo.js`;
        
        const appendScript = () => {
            if (null === head) {
                return;
            }

            head.append(scriptElement);
        }

        setTimeout(
            appendScript,
            5
        );
    }
}

export { MatomoTracking };