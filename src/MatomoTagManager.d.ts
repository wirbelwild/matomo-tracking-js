/**
 * Bit&Black Matomo Tracking
 *
 * @copyright Copyright (c) Bit&Black
 * @author Tobias Köngeter <hello@bitandblack.com>
 * @link https://www.bitandblack.com
 */
declare global {
    interface Window {
        _mtm: any;
    }
}
/**
 * Matomo Tracking
 */
declare class MatomoTagManager {
    private config;
    /**
     * @param containerId The Container Id given by Matomo, for example `container_Yolo25`.
     * @param matomoRoot  The root URL to Matomo, for example `https://www.mysite.com/matomo/` or `https://matomo.mysite.com/`.
     * @param options     Options to configure the Tag Manager.
     * @constructor
     */
    constructor(containerId: string, matomoRoot: string, options?: object);
}
export { MatomoTagManager };
