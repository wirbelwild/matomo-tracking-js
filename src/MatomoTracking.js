"use strict";
/**
 * Bit&Black Matomo Tracking
 *
 * @copyright Copyright (c) Bit&Black
 * @author Tobias Köngeter <hello@bitandblack.com>
 * @link https://www.bitandblack.com
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.MatomoTracking = void 0;
/**
 * Matomo Tracking
 */
var MatomoTracking = /** @class */ (function () {
    /**
     * @param pageId     The Page Id given by Matomo, for example `1`.
     * @param matomoRoot The root URL to Matomo, for example `https://www.mysite.com/matomo/` or `https://matomo.mysite.com/`.
     * @param options    Options to configure the Tracker.
     * @constructor
     */
    function MatomoTracking(pageId, matomoRoot, options) {
        if (options === void 0) { options = {}; }
        this.config = {};
        matomoRoot = matomoRoot.replace(/\/+$/, "");
        this.config.trackPageView = true;
        this.config.enableLinkTracking = true;
        this.config.setTrackerUrl = "".concat(matomoRoot, "/matomo.php");
        this.config.setSiteId = pageId;
        var _paq = window._paq || [];
        var scriptElement = document.createElement("script");
        var head = document.querySelector("head");
        Object.assign(this.config, options);
        for (var prop in this.config) {
            if (!this.config.hasOwnProperty(prop)) {
                continue;
            }
            var value = this.config[String(prop)];
            if (value === true) {
                _paq.push([prop]);
            }
            else if (value !== null) {
                _paq.push([prop, value]);
            }
        }
        window._paq = _paq;
        scriptElement.type = "text/javascript";
        scriptElement.async = true;
        scriptElement.src = "".concat(matomoRoot, "/matomo.js");
        var appendScript = function () {
            if (null === head) {
                return;
            }
            head.append(scriptElement);
        };
        setTimeout(appendScript, 5);
    }
    return MatomoTracking;
}());
exports.MatomoTracking = MatomoTracking;
