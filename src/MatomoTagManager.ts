/**
 * Bit&Black Matomo Tracking
 *
 * @copyright Copyright (c) Bit&Black
 * @author Tobias Köngeter <hello@bitandblack.com>
 * @link https://www.bitandblack.com
 */

declare global {
    interface Window {
        _mtm: any;
    }
}

/**
 * Matomo Tracking
 */
class MatomoTagManager {
    
    private config: any = {};
    
    /**
     * @param containerId The Container Id given by Matomo, for example `container_Yolo25`.
     * @param matomoRoot  The root URL to Matomo, for example `https://www.mysite.com/matomo/` or `https://matomo.mysite.com/`.
     * @param options     Options to configure the Tag Manager.
     * @constructor
     */
    constructor(containerId: string, matomoRoot: string, options: object = {})
    {
        matomoRoot = matomoRoot.replace(/\/+$/, "");

        const _mtm = window._mtm || [];
        const scriptElement: HTMLScriptElement = document.createElement("script");
        const head: HTMLHeadElement | null = document.querySelector("head");

        _mtm.push({
            "mtm.startTime": new Date().getTime(),
            "event": "mtm.Start",
        });

        Object.assign(this.config, options);

        for (const prop in this.config) {
            if (!this.config.hasOwnProperty(prop)) {
                continue;
            }

            const value = this.config[String(prop)];
            
            if (value === true) {
                _mtm.push([prop]);
            } else if (value !== null) {
                _mtm.push([prop, value]);
            }
        }

        window._mtm = _mtm;

        scriptElement.type = "text/javascript";
        scriptElement.async = true;
        scriptElement.src = `${matomoRoot}/js/${containerId}.js`;
        
        const appendScript = () => {
            if (null === head) {
                return;
            }

            head.append(scriptElement);
        }

        setTimeout(
            appendScript,
            5
        );
    }
}

export { MatomoTagManager };