/**
 * Bit&Black Matomo Tracking
 *
 * @copyright Copyright (c) Bit&Black
 * @author Tobias Köngeter <hello@bitandblack.com>
 * @link https://www.bitandblack.com
 */
declare global {
    interface Window {
        _paq: any;
    }
}
/**
 * Matomo Tracking
 */
declare class MatomoTracking {
    private config;
    /**
     * @param pageId     The Page Id given by Matomo, for example `1`.
     * @param matomoRoot The root URL to Matomo, for example `https://www.mysite.com/matomo/` or `https://matomo.mysite.com/`.
     * @param options    Options to configure the Tracker.
     * @constructor
     */
    constructor(pageId: number, matomoRoot: string, options?: object);
}
export { MatomoTracking };
