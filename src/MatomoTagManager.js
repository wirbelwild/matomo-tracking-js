"use strict";
/**
 * Bit&Black Matomo Tracking
 *
 * @copyright Copyright (c) Bit&Black
 * @author Tobias Köngeter <hello@bitandblack.com>
 * @link https://www.bitandblack.com
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.MatomoTagManager = void 0;
/**
 * Matomo Tracking
 */
var MatomoTagManager = /** @class */ (function () {
    /**
     * @param containerId The Container Id given by Matomo, for example `container_Yolo25`.
     * @param matomoRoot  The root URL to Matomo, for example `https://www.mysite.com/matomo/` or `https://matomo.mysite.com/`.
     * @param options     Options to configure the Tag Manager.
     * @constructor
     */
    function MatomoTagManager(containerId, matomoRoot, options) {
        if (options === void 0) { options = {}; }
        this.config = {};
        matomoRoot = matomoRoot.replace(/\/+$/, "");
        var _mtm = window._mtm || [];
        var scriptElement = document.createElement("script");
        var head = document.querySelector("head");
        _mtm.push({
            "mtm.startTime": new Date().getTime(),
            "event": "mtm.Start",
        });
        Object.assign(this.config, options);
        for (var prop in this.config) {
            if (!this.config.hasOwnProperty(prop)) {
                continue;
            }
            var value = this.config[String(prop)];
            if (value === true) {
                _mtm.push([prop]);
            }
            else if (value !== null) {
                _mtm.push([prop, value]);
            }
        }
        window._mtm = _mtm;
        scriptElement.type = "text/javascript";
        scriptElement.async = true;
        scriptElement.src = "".concat(matomoRoot, "/js/").concat(containerId, ".js");
        var appendScript = function () {
            if (null === head) {
                return;
            }
            head.append(scriptElement);
        };
        setTimeout(appendScript, 5);
    }
    return MatomoTagManager;
}());
exports.MatomoTagManager = MatomoTagManager;
