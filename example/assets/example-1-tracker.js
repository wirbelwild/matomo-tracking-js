import "../assets/example.scss";
import { MatomoTracking } from "../../index";

const matomoTracking = new MatomoTracking(
    1,
    "https://matomo.bitandblack.com",
    {
        enableHeartBeatTimer: 5
    }
);