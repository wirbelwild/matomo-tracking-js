"use strict";
/*!
 * Bit&Black Matomo Tracking
 *
 * @copyright Copyright (c) Bit&Black
 * @author Tobias Köngeter <hello@bitandblack.com>
 * @link https://www.bitandblack.com
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.MatomoTracking = exports.MatomoTagManager = void 0;
var MatomoTagManager_1 = require("./src/MatomoTagManager");
Object.defineProperty(exports, "MatomoTagManager", { enumerable: true, get: function () { return MatomoTagManager_1.MatomoTagManager; } });
var MatomoTracking_1 = require("./src/MatomoTracking");
Object.defineProperty(exports, "MatomoTracking", { enumerable: true, get: function () { return MatomoTracking_1.MatomoTracking; } });
